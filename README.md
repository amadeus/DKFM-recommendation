# Deep Knowledge Factorization Machines

A TensorFlow implementation of DKFM model in [Location Embeddings For Next Trip Recommendation](https://dl.acm.org/doi/10.1145/3308560.3316535). In Proceedings of Location on the Web (LocWeb'19).

## Environment Settings
We use Tensorflow: 
- tensorflow-gpu version:  '1.2.1'

## Dateset
DKFM model can be used for any dataset for item recommendation, one can use [Movielens Dataset](https://grouplens.org/datasets/movielens/).
These are the steps for data-preprocessing:
 - Create a data directory where you will have all the data useful for dkfm model.
 - A csv file that contains the interaction user-item and the contextual data you want to use.
 - Matrices (Numpy format) containing the textual embeddings and the kge embeddings of the items

## Example to run the code
You sould use the shell script file to run the code, the argument are clearly explained in this [file](https://gitlab.eurecom.fr/amadeus/DKFM-recommendation/blob/master/src/main.py)
You can choose the model you want to run using the argument model and then choose the hyper-parameter you want to use for the chosen model.

Run DKFM:
```
#!/usr/bin/env bash
CUDA_VISIBLE_DEVICES=0 python Gen_Test.py --model DKFM --user Demographics --item KGE_WIKI --data_dir ../data/ --Ns 3 --factors_nb 8 --nb_round 5 --embedding_size 128 --nb_epoch 6
```
