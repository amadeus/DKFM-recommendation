import numpy as np
import tensorflow as tf
import random
import pandas as pd



def fm(m, k=10):

    # design matrix
    X = tf.placeholder('float', shape=[None, m])
    # target vector
    y = tf.placeholder('float', shape=[None, 1])

    # bias and weights
    w0 = tf.Variable(tf.zeros([1]))
    W = tf.Variable(tf.zeros([m]))

    # interaction factors, randomly initialized 
    V = tf.Variable(tf.random_normal([k, m], stddev=0.01))
    
    linear_terms = tf.add(w0, tf.reduce_sum(tf.multiply(W, X), 1, keep_dims=True))
    pair_interactions = (tf.multiply(0.5,
                        tf.reduce_sum(
                            tf.subtract(
                                tf.pow( tf.matmul(X, tf.transpose(V)), 2),
                                tf.matmul(tf.pow(X, 2), tf.transpose(tf.pow(V, 2)))),
                            1, keep_dims=True)))
    y_hat = tf.add(linear_terms, pair_interactions)
    
 
    lambda_w = tf.constant(0.001, name='lambda_w')
    lambda_v = tf.constant(0.001, name='lambda_v')

    l2_norm = (tf.reduce_sum(
                tf.add(
                    tf.multiply(lambda_w, tf.pow(W, 2)),
                    tf.multiply(lambda_v, tf.pow(V, 2)))))

    error = tf.reduce_mean(tf.square(tf.subtract(y, y_hat)))
    
    fm_loss = tf.add(error, l2_norm)
    
    
    
    
    train_op = tf.train.AdamOptimizer(learning_rate=0.01).minimize(fm_loss)
    
    return X, y, y_hat, fm_loss, train_op, W, V



def bpr_mf(nb_user, nb_item, lr, hidden_dim):
    u = tf.placeholder(tf.int32, [None])
    i = tf.placeholder(tf.int32, [None])
    j = tf.placeholder(tf.int32, [None])


    user_emb_w = tf.get_variable("user_emb_w", [nb_user, hidden_dim], 
                        initializer=tf.random_normal_initializer(0, 0.1))
    item_emb_w = tf.get_variable("item_emb_w", [nb_item, hidden_dim], 
                            initializer=tf.random_normal_initializer(0, 0.1))
    item_b = tf.get_variable("item_b", [nb_item+1, 1], 
                            initializer=tf.constant_initializer(0.0))

    u_emb = tf.nn.embedding_lookup(user_emb_w, u)
    i_emb = tf.nn.embedding_lookup(item_emb_w, i)
    i_b = tf.nn.embedding_lookup(item_b, i)
    j_emb = tf.nn.embedding_lookup(item_emb_w, j)
    j_b = tf.nn.embedding_lookup(item_b, j)
    
    # MF predict: u_i > u_j
    x = i_b - j_b + tf.reduce_sum(tf.multiply(u_emb, (i_emb - j_emb)), 1, keep_dims=True)
    
    # if x > 0 1 else 0
    tr_val = tf.reduce_mean(x)
    
    l2_norm = tf.add_n([
            tf.reduce_sum(tf.multiply(u_emb, u_emb)), 
            tf.reduce_sum(tf.multiply(i_emb, i_emb)),
            tf.reduce_sum(tf.multiply(j_emb, j_emb))
        ])
    
    lamnbda = 0.0001
    bprloss = lamnbda * l2_norm - tf.reduce_mean(tf.log(tf.sigmoid(x)))
    
    train_op = tf.train.AdamOptimizer(lr).minimize(bprloss)
    return u, i, j, x, tr_val, bprloss, train_op, user_emb_w, item_emb_w



def mlp_x(nb_user, nb_item, hidden_dim, nb_layers=3 , lr = 0.005):
    
    ## tf Graph input
    X_user = tf.placeholder(tf.int32, shape=[None], name="X_user")
    X_item = tf.placeholder(tf.int32, shape = [None],name="X_item")
    Y_labels = tf.placeholder(dtype=tf.float32, shape=[None,1],name="Y_labels")


    ## Embedding Layer for each of user and iten
    user_emb_w = tf.get_variable("user_emb_w", [nb_user, hidden_dim], 
                        initializer=tf.random_normal_initializer(0, 0.1))
    item_emb_w = tf.get_variable("item_emb_w", [nb_item, hidden_dim], 
                            initializer=tf.random_normal_initializer(0, 0.1))


    u_emb = tf.nn.embedding_lookup(user_emb_w, X_user)
    i_emb = tf.nn.embedding_lookup(item_emb_w, X_item)
    
    
    ## Concatenation of the two embedded vector
    concat = tf.concat([u_emb, i_emb],axis=1)

    weights = dict()
    biases = dict()
    k = 1
    layer = concat
    for l in range(nb_layers):
        weights['h'+str(l)] = tf.Variable(tf.random_normal([layer.get_shape().as_list()[1], int(2**(2*nb_layers)/k)]))
        biases['b'+str(l)] = tf.Variable(tf.random_normal([int(2**(2*nb_layers)/k)]))
        k = 2*k
        layer = tf.add(tf.matmul(layer, weights['h'+str(l)]), biases['b'+str(l)])
        layer = tf.nn.dropout(layer, keep_prob = 0.75)
        layer = tf.nn.relu(layer)
    
    result_MLP = layer
    
    ## output Layer
    h_MLP = tf.Variable(tf.random_normal([result_MLP.get_shape().as_list()[1], 1]))
    b_MLP = tf.Variable(tf.random_normal([1]))
    
    result = tf.matmul(result_MLP, h_MLP) + b_MLP
    
    
    ## Optim part
    logits = result
    loss_op = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=logits, labels=Y_labels))
    optimizer = tf.train.AdamOptimizer(learning_rate=lr)
    train_op = optimizer.minimize(loss_op)


    return X_user, X_item, Y_labels, loss_op, train_op, user_emb_w, item_emb_w, logits



def gmf(nb_user, nb_item, hidden_dim=16, lr = 0.005):
    
    ## tf Graph input
    X_user = tf.placeholder(tf.int32, shape=[None], name="X_user")
    X_item = tf.placeholder(tf.int32, shape = [None],name="X_item")
    Y_labels = tf.placeholder(dtype=tf.float32, shape=[None,1],name="Y_labels")


    ## Embedding Layer for each of user and iten
    user_emb_w = tf.get_variable("user_emb_w", [nb_user, hidden_dim], 
                        initializer=tf.random_normal_initializer(0, 0.1))
    item_emb_w = tf.get_variable("item_emb_w", [nb_item, hidden_dim], 
                            initializer=tf.random_normal_initializer(0, 0.1))


    u_emb = tf.nn.embedding_lookup(user_emb_w, X_user)
    i_emb = tf.nn.embedding_lookup(item_emb_w, X_item)
    
    
    
    ## Element wise Multiplication the first part of the framework
    result_GMF = tf.multiply(u_emb,i_emb)
    
    
    h = tf.Variable(tf.random_normal([result_GMF.get_shape().as_list()[1], 1]))
    b = tf.Variable(tf.random_normal([1]))

    FC_layer = tf.add(tf.matmul(result_GMF, h),b)
    
    
    ## Optim part
    logits = FC_layer
    loss_op = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=logits, labels=Y_labels))
    optimizer = tf.train.AdamOptimizer(learning_rate=lr)
    train_op = optimizer.minimize(loss_op)


    return X_user, X_item, Y_labels, loss_op, train_op, user_emb_w, item_emb_w, logits

def deep_model(nb_user, nb_item, tr_data, hidden_dim=16, dim_layer = 256, nb_layers=3, lr = 0.005, additional_data='None'):
    
    ## tf Graph input
    X_user = tf.placeholder(tf.int32, shape=[None], name="X_user")
    X_item = tf.placeholder(tf.int32, shape = [None],name="X_item")
    if additional_data == 'user':
        user_item_features = tf.placeholder(tf.float32, shape = [None, tr_data.shape[1] - 3],name="user_features")
    elif additional_data == 'item':
        user_item_features = tf.placeholder(tf.float32, shape = [None, tr_data.shape[1] - 3],name="item_features")
    elif additional_data == 'user_item':
        user_item_features = tf.placeholder(tf.float32, shape = [None, tr_data.shape[1] - 3],name="user_item_features")
        
    Y_labels = tf.placeholder(dtype=tf.float32, shape=[None,1],name="Y_labels")


    ## Embedding Layer for each of user and iten
    user_emb_w = tf.get_variable("user_emb_w", [nb_user, hidden_dim], 
                        initializer=tf.random_normal_initializer(0, 0.1))
    item_emb_w = tf.get_variable("item_emb_w", [nb_item, hidden_dim], 
                            initializer=tf.random_normal_initializer(0, 0.1))


    u_emb = tf.nn.embedding_lookup(user_emb_w, X_user)
    i_emb = tf.nn.embedding_lookup(item_emb_w, X_item)
    
    
    ## Concatenation of the two embedded vector
    if additional_data=='None':
        concat = tf.concat([u_emb, i_emb],axis=1)
    else :
        concat = tf.concat([u_emb, i_emb, user_item_features],axis=1)
        
    weights = dict()
    biases = dict()
    k = 1
    layer = concat
    for l in range(nb_layers):
        weights['h'+str(l)] = tf.Variable(tf.random_normal([layer.get_shape().as_list()[1], int(dim_layer/k)]))
        biases['b'+str(l)] = tf.Variable(tf.random_normal([int(dim_layer/k)]))
        k = 2*k
        layer = tf.add(tf.matmul(layer, weights['h'+str(l)]), biases['b'+str(l)])
        layer = tf.nn.dropout(layer, keep_prob = 0.75)
        layer = tf.nn.relu(layer)
    
    result_MLP = layer
    
    ## output Layer
    h_MLP = tf.Variable(tf.random_normal([result_MLP.get_shape().as_list()[1], 1]))
    b_MLP = tf.Variable(tf.random_normal([1]))
    
    result = tf.matmul(result_MLP, h_MLP) + b_MLP
    
    
    ## Optim part
    logits = result
    loss_op = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=logits, labels=Y_labels))
    optimizer = tf.train.AdamOptimizer(learning_rate=lr)
    train_op = optimizer.minimize(loss_op)

    if additional_data == 'None':
        return X_user, X_item, Y_labels, loss_op, train_op, user_emb_w, item_emb_w, logits
    else :
        return X_user, X_item, user_item_features, Y_labels, loss_op, train_op, user_emb_w, item_emb_w, logits
    
    
def dkfm(nb_user, nb_item, tr_data, hidden_dim=16, dim_layer = 256, nb_layers=3, lr = 0.005, k = 10,dim_ctxt = 17, additional_data='None'):
    
    ## tf Graph input
    X_user = tf.placeholder(tf.int32, shape=[None], name="X_user")
    X_item = tf.placeholder(tf.int32, shape = [None],name="X_item")
    X_ctxt = tf.placeholder(tf.float32, shape = [None, dim_ctxt],name="X_ctx")
    if additional_data == 'user':
        user_item_features = tf.placeholder(tf.float32, shape = [None, tr_data.shape[1] - 3 - dim_ctxt],name="user_features")
    elif additional_data == 'item':
        user_item_features = tf.placeholder(tf.float32, shape = [None, tr_data.shape[1] - 3 - dim_ctxt],name="item_features")
    elif additional_data == 'user_item':
        user_item_features = tf.placeholder(tf.float32, shape = [None, tr_data.shape[1] - 3 - dim_ctxt],name="user_item_features")
        
    Y_labels = tf.placeholder(dtype=tf.float32, shape=[None,1],name="Y_labels")


    ## Embedding Layer for each of user and iten
    user_emb_w = tf.get_variable("user_emb_w", [nb_user, hidden_dim], 
                        initializer=tf.random_normal_initializer(0, 0.1))
    item_emb_w = tf.get_variable("item_emb_w", [nb_item, hidden_dim], 
                            initializer=tf.random_normal_initializer(0, 0.1))


    u_emb = tf.nn.embedding_lookup(user_emb_w, X_user)
    i_emb = tf.nn.embedding_lookup(item_emb_w, X_item)
    
    
    # interaction factors, randomly initialized 
    V = tf.Variable(tf.random_normal([k, dim_ctxt], stddev=0.01))
    
    #*****************#
    
    # Calculate output with FM equation
    pool_layer = tf.multiply(0.5,
                            tf.subtract(
                                tf.pow( tf.matmul(X_ctxt, tf.transpose(V)), 2),
                                tf.matmul(tf.pow(X_ctxt, 2), tf.transpose(tf.pow(V, 2)))))
    
    ## Concatenation of the two embedded vector
        ## Concatenation of the two embedded vector
    if additional_data=='None':
        concat = tf.concat([u_emb, i_emb, pool_layer],axis=1)
    else :
        concat = tf.concat([u_emb, i_emb, user_item_features, pool_layer],axis=1)
    
    
    print(concat.shape)
    weights = dict()
    biases = dict()
    k = 1
    layer = concat
    for l in range(nb_layers):
        weights['h'+str(l)] = tf.Variable(tf.random_normal([layer.get_shape().as_list()[1], int(dim_layer/k)]))
        biases['b'+str(l)] = tf.Variable(tf.random_normal([int(dim_layer/k)]))
        k = 2*k
        layer = tf.add(tf.matmul(layer, weights['h'+str(l)]), biases['b'+str(l)])
        layer = tf.nn.dropout(layer, keep_prob = 0.75)
        layer = tf.nn.relu(layer)
    
    result_MLP = layer
    
    ## output Layer
    h_MLP = tf.Variable(tf.random_normal([result_MLP.get_shape().as_list()[1], 1]))
    b_MLP = tf.Variable(tf.random_normal([1]))
    
    result = tf.matmul(result_MLP, h_MLP) + b_MLP
    
    
    ## Optim part
    logits = result
    loss_op = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=logits, labels=Y_labels))
    optimizer = tf.train.AdamOptimizer(learning_rate=lr)
    train_op = optimizer.minimize(loss_op)

    if additional_data == 'None':
        return X_user, X_item, X_ctxt, Y_labels, loss_op, train_op, user_emb_w, item_emb_w, logits
    else :
        return X_user, X_item, user_item_features, X_ctxt, Y_labels, loss_op, train_op, user_emb_w, item_emb_w, logits 