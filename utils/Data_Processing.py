import tensorflow as tf
import pandas as pd
import random
import numpy as np
import sys
sys.path.append('./utils/')
from Data_Processing import *
from  Models import *
from Utils import *


def load_test_data(interaction_matrix):
    
    '''
        interaction matrix : It represents the occurence matrix where each element is the occurence of the pair (user, item) [Pandas DF]
    '''
    
    ## This is 
    nb_item = len(np.unique(interaction_matrix.values[:,2]))
    nb_user = len(np.unique(interaction_matrix.values[:,1]))
    
    ## Each element in test_dict contains the pair (user, test_item) following the LOO procedure
    test_dict = dict()
    
    ## Contains the index row in interaction matrix for training data
    droped_index = list()
    
    ## Sort matrix by user index
    interaction_matrix_sorted = interaction_matrix.sort_values(['user_id'])

    ## Matrix containing the user and item index
    fp = interaction_matrix_sorted[['user_id', 'item_id', 'Occurences']].values.astype(int)
    
    ## Loop for in order to compute the test dictionary
    aux = 0
    for i in range(len(fp)):
        if fp[i,0] != aux:
            droped_index.append(i-1)
            test_dict[str(fp[i-1,0])] = str(fp[i-1,1])
            aux = fp[i,0]
    test_dict[str(fp[i,0])] = str(fp[i,1])
    droped_index.append(i)

    return test_dict, droped_index, fp
    
    
def load_training_data(droped_index, fp):
    
    '''
        dropped index : the index to remove from the interaction matrix to consider only training data
    '''
    
    ## The dataframe contains all the values \ the test data
    fp_tr = np.delete(fp, droped_index, 0)
    
    tr_dict = dict()
    for i in range(len(fp_tr)):
        user_id = fp_tr[i,0]
        item_id = fp_tr[i,1]
        if not(str(user_id) in tr_dict):
            tr_dict[str(user_id)] = [str(item_id)]
        else :
            list_appended = tr_dict[str(user_id)]
            list_appended.append(str(item_id))
            tr_dict[str(user_id)] = list_appended
    
    return tr_dict

def ctxt_data(u, i, item_index, user_index, dict_ui_cxt, dim_cxt):
    key = str(user_index[int(u)]) + '_' + str(item_index[int(i)])
    if key in dict_ui_cxt:
        return dict_ui_cxt[str(user_index[int(u)]) + '_' + str(item_index[int(i)])]
    else : 
        vec = np.zeros(dim_cxt)
        vec[random.randint(0,9)] = 1
        vec[random.randint(10,16)]= 1
        return np.reshape(vec, (1,-1))
    
    return dict_ui_cxt[str(user_index[int(u)]) + '_' + str(item_index[int(i)])]


def additional_info(u , i, user_charac, item_charac, additional_data = 'user_item'):
    
    if additional_data == 'user_item':
        additional_info = np.concatenate([np.reshape(user_charac[int(u),:], (1,-1)), np.reshape(item_charac[int(i),:], (1,-1))],axis=1)
    elif additional_data == 'item':
        additional_info = np.reshape(item_charac[int(i),:], (1,-1))
    elif additional_data == 'user':
        additional_info = np.reshape(user_charac[int(u),:], (1,-1))
    return additional_info

def generate_train_data(tr_dict, test_dict, nb_user, nb_item, user_charac, item_charac, framework = 'NCF', nb_neg_sample = 3, additional_data = 'user_item', item_index=0, user_index=0, dict_ui_cxt=0, dim_cxt=0):
    
    if framework == 'DL': 
        if additional_data == 'None':        
            t = []
            for u in tr_dict:
                for i in tr_dict[u]:
                    ti = [int(u), int(i), int(1)]
                    t.append(ti)
                    for k in range(nb_neg_sample):
                        j = random.randint(0,nb_item-1)
                        while (str(j) in tr_dict[str(u)]) or (str(j) == test_dict[str(u)]):
                            j = random.randint(0,nb_item-1)
                        tj = [int(u), int(j), int(0)]
                        t.append(tj)
            t_return = np.array(t)
        else:
            t = []
            for u in tr_dict:
                for i in tr_dict[u]:
                    ti = np.concatenate([np.reshape([int(u), int(i)], (1,-1)), additional_info(u, i, user_charac, item_charac, additional_data), np.reshape([int(1)], (1,-1))], axis = 1)
                    t.append(ti)
                    for k in range(nb_neg_sample):
                        j = random.randint(0,nb_item-1)
                        while (str(j) in tr_dict[str(u)]) or (str(j) == test_dict[str(u)]):
                            j = random.randint(0,nb_item-1)
                        tj = np.concatenate([np.reshape([int(u), int(j)], (1,-1)), additional_info(u, j, user_charac,  item_charac, additional_data), np.reshape([int(0)], (1,-1))], axis = 1)
                        t.append(tj)
            t_return = np.array(t)[:,0,:]
                
    elif framework == 'DKFM':
        
        if additional_data == 'None':
            t = []
            for u in tr_dict:
                for i in tr_dict[u]:
                    ti = np.concatenate([np.reshape([int(u), int(i)], (1,-1)), ctxt_data(u, i, item_index, user_index, dict_ui_cxt, dim_cxt), np.reshape([int(1)], (1,-1))], axis = 1)
                    t.append(ti)
                    for k in range(nb_neg_sample):
                        j = random.randint(0,nb_item-1)
                        while (str(j) in tr_dict[str(u)]) or (str(j) == test_dict[str(u)]):
                            j = random.randint(0,nb_item-1)
                        tj = np.concatenate([np.reshape([int(u), int(j)], (1,-1)), ctxt_data(u, j, item_index, user_index, dict_ui_cxt, dim_cxt), np.reshape([int(0)], (1,-1))], axis = 1)
                        t.append(tj)
            t_return = np.array(t)[:,0,:]
        else:
            t = []
            for u in tr_dict:
                for i in tr_dict[u]:
                    ti = np.concatenate([np.reshape([int(u), int(i)], (1,-1)), additional_info(u, i, user_charac, item_charac, additional_data), ctxt_data(u, i, item_index, user_index, dict_ui_cxt, dim_cxt), np.reshape([int(1)], (1,-1))], axis = 1)
                    t.append(ti)
                    for k in range(nb_neg_sample):
                        j = random.randint(0,nb_item-1)
                        while (str(j) in tr_dict[str(u)]) or (str(j) == test_dict[str(u)]):
                            j = random.randint(0,nb_item-1)
                        tj = np.concatenate([np.reshape([int(u), int(j)], (1,-1)), additional_info(u, j, user_charac,  item_charac, additional_data), ctxt_data(u, j, item_index, user_index, dict_ui_cxt, dim_cxt), np.reshape([int(0)], (1,-1))], axis = 1)
                        t.append(tj)
            t_return = np.array(t)[:,0,:]
    
    elif framework == 'NCF':
        t = []
        for u in tr_dict:
            for i in tr_dict[u]:
                ti = [int(u), int(i), int(1)]
                t.append(ti)
                for k in range(nb_neg_sample):
                    j = random.randint(0,nb_item-1)
                    while (str(j) in tr_dict[str(u)]) or (str(j) == test_dict[str(u)]):
                        j = random.randint(0,nb_item-1)
                    tj = [int(u), int(j), int(0)]
                    t.append(tj)
        t_return = np.array(t)
        
    elif framework == 'BPRMF' : 
        t = []
        for u in tr_dict:
            for i in tr_dict[u]:
                for k in range(nb_neg_sample):
                    j = random.randint(0,nb_item-1)
                    while (str(j) in tr_dict[str(u)]) or (str(j) == test_dict[str(u)]):
                        j = random.randint(0,nb_item-1)
                    tj = [int(u), int(i), int(j)]
                    t.append(tj)
        t_return = np.array(t)
    
    return t_return
 
def next_batch(tr_data, zeros, ones, tr_dict, batch_size = 256):
    tr_data_batch = np.zeros((2*batch_size, tr_data.shape[1]))
    k = 0
    for i in range(batch_size):
        index_one = random.choice(ones)
        index_zero = random.choice(zeros)
        tr_data_batch[k,:] = tr_data[index_one,:]
        tr_data_batch[k+1,:] = tr_data[index_zero,:]
        k += 2
    return tr_data_batch                                       
                                                           
                                                           
def generate_test_data(tr_dict, test_dict, nb_user, nb_item, user_charac, item_charac, framework = 'NCF', additional_data = 'user_item', item_index=0, user_index=0, dict_ui_cxt=0, dim_cxt=0):

                                                           
    if framework == 'DL': 
        if additional_data == 'None':    
            t_gen = []
            for u in test_dict:
                t = []
                i = test_dict[str(u)]
                ti = [int(u), int(i), int(1)]
                t.append(ti)
                for j in range(0, nb_item):
                    if not (str(j) in tr_dict[str(u)]) and int(i) != j:
                        tj = [int(u), int(j), int(0)]
                        t.append(tj)
                t_gen.append(t)        
        else :    
            t_gen = []
            for u in test_dict:
                t = []
                i = test_dict[str(u)]
                ti = np.concatenate([np.reshape([int(u), int(i)], (1,-1)), additional_info(u, i, user_charac, item_charac, additional_data), np.reshape([int(1)], (1,-1))], axis = 1)
                t.append(ti)
                for j in range(nb_item):
                    if not (str(j) in tr_dict[str(u)]) and i != j:
                        tj = np.concatenate([np.reshape([int(u), int(j)], (1,-1)), additional_info(u, j, user_charac, item_charac, additional_data), np.reshape([int(0)], (1,-1))], axis = 1)
                        t.append(tj)
                t_gen.append(t)
            
    elif framework == 'DKFM':
        
        if additional_data == 'None':
            t_gen = []
            for u in test_dict:
                t = []
                i = test_dict[str(u)]
                ti = np.concatenate([np.reshape([int(u), int(i)], (1,-1)), ctxt_data(u, i, item_index, user_index, dict_ui_cxt, dim_cxt), np.reshape([int(1)], (1,-1))], axis = 1)
                t.append(ti)
                for j in range(nb_item):
                    if not (str(j) in tr_dict[str(u)]) and i != j:
                        tj = np.concatenate([np.reshape([int(u), int(j)], (1,-1)), ctxt_data(u, j, item_index, user_index, dict_ui_cxt, dim_cxt), np.reshape([int(0)], (1,-1))], axis = 1)
                        t.append(tj)
                t_gen.append(t)
        else : 
            t_gen = []
            for u in test_dict:
                t = []
                i = test_dict[str(u)]
                ti = np.concatenate([np.reshape([int(u), int(i)], (1,-1)), additional_info(u, i, user_charac, item_charac, additional_data ), ctxt_data(u, i, item_index, user_index, dict_ui_cxt, dim_cxt), np.reshape([int(1)], (1,-1))], axis = 1)
                t.append(ti)
                for j in range(nb_item):
                    if not (str(j) in tr_dict[str(u)]) and i != j:
                        tj = np.concatenate([np.reshape([int(u), int(j)], (1,-1)), additional_info(u, j, user_charac, item_charac, additional_data), ctxt_data(u, j, item_index, user_index, dict_ui_cxt, dim_cxt), np.reshape([int(0)], (1,-1))], axis = 1)
                        t.append(tj)
                t_gen.append(t)
    
    elif framework == 'NCF':
        t_gen = []
        for u in test_dict:
            t = []
            i = test_dict[str(u)]
            ti = [int(u), int(i), int(1)]
            t.append(ti)
            for j in range(0, nb_item):
                if not (str(j) in tr_dict[str(u)]) and int(i) != j:
                    tj = [int(u), int(j), int(0)]
                    t.append(tj)
            t_gen.append(t)   
            
    elif framework == 'BPRMF':
        t_gen = []
        for u in test_dict:
            t = []
            i = test_dict[str(u)]
            for j in range(nb_item):
                if not (str(j) in tr_dict[str(u)]) and int(i) != int(j):
                    t.append([int(u), int(i), int(j)])
            t_gen.append(np.asarray(t))
    
                                                           
    return np.asarray(t_gen)    