import numpy as np
import pickle

def sigmoid(x):
    return (1/(1+np.exp(-x)))

def load_obj(name):
    with open(name, 'rb') as f:
        return pickle.load(f)