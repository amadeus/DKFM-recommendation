import numpy as np
import tensorflow as tf
import random
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.manifold import TSNE
from sklearn.preprocessing import MinMaxScaler, normalize
import sys
sys.path.append('../utils/')
from Data_Processing import *
from  Models import *
from Utils import *
import timeit
import os
import argparse




class Dataset:
    
    def __init__(self, data_dir, framework, user_data, item_data):
        self.framework = framework
        self.data_dir = data_dir
        self.interaction_matrix = pd.read_csv(self.data_dir + 'path to interaction file in csv format')
        self.item_index = np.load(self.data_dir + 'path to numpy file containing item index')
        self.user_index = np.load(self.data_dir + 'path to numpy file containing user index')
        self.test_dict, droped_index, interaction_matrix_sorted = load_test_data(self.interaction_matrix)
        self.tr_dict = load_training_data(droped_index, interaction_matrix_sorted)
        self.nb_item = len(self.item_index)
        self.nb_user = len(self.user_index)
        self.user_data = user_data
        self.item_data = item_data
        
        if self.item_data == 'None' and self.user_data == 'None' :    
            self.additional_data = 'None'
        elif (self.item_data == 'None') and self.user_data=='Demographics' :
            self.additional_data = 'user' 
        elif (self.item_data != 'None') and self.user_data=='None' :
            self.additional_data = 'item'    
        elif (self.item_data != 'None') and self.user_data=='Demographics' :
            self.additional_data = 'user_item'
        
        if framework == 'DKFM':
            self.dict_ui_ctx = load_obj(self.data_dir + 'path to dictionary containing contextual data')    
            self.dim_ctxt = list(dict_ui_ctx.keys())[0]
        else :
            self.dict_ui_ctx = 0
            self.dim_ctxt = 0
        if framework == 'BPRMF':
            self.user_charac = 0
            self.item_charac = 0
        else: 
            ## User Data
            if self.user_data == 'Demographics':
                self.user_charac = np.load(self.data_dir + "path to numpy matrix containing user demographics data")
                self.user_charac = self.user_charac[:,1:self.user_charac.shape[1]]
                self.user_charac = normalize(self.user_charac.astype(float), norm='l2', axis=1)
            else:
                self.user_charac = 0
            # Item Data    
            if self.item_data == 'None':
                self.item_charac = 0
            elif self.item_data == 'KGE':
                self.item_charac = np.load(self.data_dir + 'path to numpy matrix containing cities kge')
                self.item_charac = normalize(self.item_charac.astype(float), norm='l2', axis=1)
            elif self.item_data == 'WIKI':
                self.item_charac = np.load(self.data_dir + 'path to numpy matrix containing cities fasttext')
                self.item_charac = normalize(self.item_charac.astype(float), norm='l2', axis=1)
            elif self.item_data == 'KGE_WIKI':
                item_charac_1 = np.load(self.data_dir + 'path to numpy matrix containing cities kge')
                item_charac_2 = np.load(self.data_dir + 'path to numpy matrix containing cities fasttext')
                self.item_charac = np.concatenate([item_charac_1, item_charac_2], axis=1)
                self.item_charac = normalize(self.item_charac.astype(float), norm='l2', axis=1)
        
        if self.additional_data == 'user':
            self.dim_dfm = self.user_charac.shape[1]
        elif self.additional_data == 'item':
            self.dim_dfm = self.item_charac.shape[1]
        elif self.additional_data == 'user_item':
            self.dim_dfm = self.item_charac.shape[1] + self.user_charac.shape[1]            
    
    def build_tr_data(self, Ns):
        
        tr_data = generate_train_data(self.tr_dict, self.test_dict, self.nb_user, self.nb_item, 
                                  self.user_charac, self.item_charac, self.framework, Ns,
                                  self.additional_data, self.item_index, self.user_index,
                                  self.dict_ui_ctx, self.dim_ctxt)
        return tr_data
    
    def build_test_data(self):
        
        t_test = generate_test_data(self.tr_dict, self.test_dict, self.nb_user, self.nb_item, 
                                  self.user_charac, self.item_charac, self.framework,
                                  self.additional_data, self.item_index, self.user_index,
                                  self.dict_ui_ctx, self.dim_ctxt)
        return t_test
        