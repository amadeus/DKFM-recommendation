#!/usr/bin/env bash
CUDA_VISIBLE_DEVICES=0 \
python Gen_Test.py \
--model DKFM \
--user Demographics \
--item KGE_WIKI \
--data_dir ../data/ \
--Ns 3 \
--factors_nb 8 \
--nb_round 5 \
--dim_layer_size 1024 \
--embedding_size 128 \
--batch_size 256 \
--learning_rate 0.008 \
--nb_epoch 6