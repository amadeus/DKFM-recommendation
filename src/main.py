import numpy as np
import tensorflow as tf
import random
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.manifold import TSNE
from sklearn.preprocessing import MinMaxScaler, normalize
import sys
sys.path.append('../utils/')
from Data_Processing import *
from Models import *
from Dataset import *
from Utils import *
from Training_Evaluation import *
import timeit
import os
import argparse

def main():
    parser = argparse.ArgumentParser(description='Models')
    '''
    Arguments:
        model: you have to choose which model you want to use for training 
        {"BPRMF", "DL", "DKFM"}
        user: if "None", no user information is used in the model, otherwise 
        you can choose "demographics"
        item: if "None", no item information is used in the model, otherwise you
        can choose "KGE" for knowledge graph embedding information or "WIKI" for 
        fasttext embedding, or both by assigning "KGE_WIKI" to item argument
        data_dir: is the directory where all the data are put
        Ns: is the number of negative sample generated
        nb_round: is the number of time you train and test your model
        embedding_size: is the size of the user and item embedding layers
        batch_size: is the number of samples for each parameter's update 
        factors_nb: is the size of the features vectors (factorization machine 
        component)
        dim_layer_size: is the size of the first layer
        mlp_layer_nb: is the number of hidden layers
        learning_rate: is the learning rate use in the model
        nb_epoch: is the number of times you do a pass over your training data
    '''
    parser.add_argument('--model', type=str, default='BPRMF')
    parser.add_argument('--user', type=str, default='None')
    parser.add_argument('--item', type=str, default='None')
    parser.add_argument('--data_dir', type=str, default='../data/')
    parser.add_argument('--Ns', type=int, default=3)
    parser.add_argument('--nb_round', type=int, default=10)
    parser.add_argument('--embedding_size', type=int, default=128)
    parser.add_argument('--batch_size', type=int, default=256)
    parser.add_argument('--factors_nb', type=int, default=8)
    parser.add_argument('--dim_layer_size', type=int, default=256)
    parser.add_argument('--mlp_layer_nb', type=int, default=2) 
    parser.add_argument('--learning_rate', type=float, default=0.006)
    parser.add_argument('--nb_epoch', type=int, default=5)
    args = parser.parse_args()
    print(args)
    
    print('*************************** Loading the Dataset: ****************************************')

    dataset = Dataset(args.data_dir, args.model, args.user, args.item)
    additional_data = dataset.additional_data
    
    print('*************************** Construct the Data ****************************************')
    
    tr_data = dataset.build_tr_data(1)
    t_test = dataset.build_test_data()


    tot_hitrate_10 = 0
    tot_mrr = 0
    tot_test_loss = 0
    
    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"]="0"
    config = tf.ConfigProto()
    config.gpu_options.allow_growth=True

    
    
    tot_hitrate_10 = 0
    tot_mrr = 0
    tot_test_loss = 0

    for i in range(args.nb_round):
        
        os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
        os.environ["CUDA_VISIBLE_DEVICES"]="0"
        config = tf.ConfigProto()
        config.gpu_options.allow_growth=True
        
        with tf.Graph().as_default(), tf.Session(config=config) as session:

            training(session, dataset, args, additional_data)
            evaluation(session, t_test, dataset, args, additional_data)
    
if __name__ == '__main__':
    main()    
