import numpy as np
import tensorflow as tf
import random
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.manifold import TSNE
from sklearn.preprocessing import MinMaxScaler, normalize
import sys
sys.path.append('../utils/')
from Data_Processing import *
from  Models import *
from Dataset import *
from Utils import *
import timeit
import os
import argparse


def training(session, dataset, args, additional_data):
    
    if args.model == 'BPRMF':
        u, i, j, x, tr_val, bprloss, train_op, user_emb_w, item_emb_w = bpr_mf(dataset.nb_user, dataset.nb_dest, lr=args.learning_rate, hidden_dim=args.embedding_size)
        
    elif args.model == 'DKFM':
        if additional_data == 'None':
            X_user, X_item, X_ctxt, Y_labels, loss_op, train_op, user_emb_w, item_emb_w, logits = dkfm(dataset.nb_user,
                    dataset.nb_dest, tr_data, hidden_dim=args.embedding_size, dim_layer = args.dim_layer_size, nb_layers=args.mlp_layer_nb,
                    lr = args.learning_rate, k = args.factors_nb, dim_ctxt = dataset.dim_ctxt, additional_data=additional_data)
        else:
            X_user, X_item, user_item_features, X_ctxt, Y_labels, loss_op, train_op, user_emb_w, item_emb_w, logits = dkfm(dataset.nb_user, dataset.nb_dest, tr_data, hidden_dim=args.embedding_size, dim_layer = args.dim_layer_size, nb_layers=args.mlp_layer_nb, lr = args.learning_rate, k = args.factors_nb, dim_ctxt = dataset.dim_ctxt, additional_data=additional_data)
            
    elif args.model == 'DL':
        if additional_data == 'None':
            X_user, X_item, Y_labels, loss_op, train_op, user_emb_w, item_emb_w, logits = deep_model(dataset.nb_user,
dataset.nb_dest, tr_data, hidden_dim=args.embedding_size, dim_layer = args.dim_layer_size, nb_layers=args.mlp_layer_nb, lr = args.learning_rate, additional_data=additional_data)
        else:
            X_user, X_item, user_item_features, Y_labels, loss_op, train_op, user_emb_w, item_emb_w, logits = deep_model(dataset.nb_user, dataset.nb_dest, tr_data, hidden_dim=args.embedding_size, dim_layer = args.dim_layer_size, nb_layers=args.mlp_layer_nb, lr = args.learning_rate, additional_data=additional_data)
    
    print('*********************** Initializing Graph For: ' + str(args.model) + '**************************')
    
    session.run(tf.global_variables_initializer())

    print('*********************** Start Training **************************')

    for epoch in range(args.nb_epoch):

        epoch += 1
        print("epoch: ", epoch)

        _batch_loss_op = 0


        tr_data = dataset.build_tr_data(args.Ns)

        batch_size = 256
        epoch_size = int(len(tr_data)/batch_size)
        n_used_data = 0

        for k in range(epoch_size): # uniform samples from training set

            t = tr_data[k*batch_size:(k+1)*batch_size,:]


            if args.model == 'BPRMF':
                _loss_op, user_emb_w_, item_emb_w_, _ = session.run([bprloss, user_emb_w, item_emb_w, train_op],
                                                                    feed_dict={u:t[:,0], i:t[:,1], j:t[:,2]})
            elif args.model == 'DKFM':
                if additional_data == 'None':
                    _loss_op, user_emb_w_, item_emb_w_, _ = session.run([loss_op, user_emb_w, item_emb_w, train_op], 
                                                                            feed_dict={X_user:t[:,0], X_item:t[:,1], X_ctxt:t[:,2:t.shape[1]-1],
                                                                                       Y_labels: np.reshape(t[:,t.shape[1]-1], (-1,1))})
                else:
                    _loss_op, user_emb_w_, item_emb_w_, _ = session.run([loss_op, user_emb_w, item_emb_w, train_op], 
                                feed_dict={X_user:t[:,0], X_item:t[:,1], user_item_features:t[:,2:t.shape[1] - dataset.dim_ctxt - 1],
                                   X_ctxt:t[:,2+dataset.dim_dfm:t.shape[1] - 1], Y_labels: np.reshape(t[:,t.shape[1]-1], (-1,1))})
            elif args.model == 'DL':
                if additional_data == 'None':
                    _loss_op, user_emb_w_, item_emb_w_, _ = session.run([loss_op, user_emb_w, item_emb_w, train_op], 
                                        feed_dict={X_user:t[:,0], X_item:t[:,1], Y_labels: np.reshape(t[:,2], (-1,1))})
                elif additional_data != 'None':
                    _loss_op, user_emb_w_, item_emb_w_, _ = session.run([loss_op, user_emb_w, item_emb_w, train_op], 
                             feed_dict={X_user:t[:,0], X_item:t[:,1], user_item_features:t[:,2:t.shape[1]-1],
                                                                              Y_labels:np.reshape(t[:,t.shape[1]-1], (len(t), 1))})

                    _batch_loss_op += _loss_op

            tr_loss_ = _batch_loss_op / k  
            print("model_loss =  ", _batch_loss_op / k)
            
def evaluation(session, t_test, dataset, args, additional_data):            
            
    hitrate_10 = 0
    mrr = 0

    _test_loss = 0
    for t in t_test:

        t = np.array(t)
                
        if args.model == 'BPRMF':
            x_, tr_val_, test_loss_op = session.run([x, tr_val, bprloss],
                                        feed_dict={u:t[:,0], i:t[:,1], j:t[:,2]})
        elif args.model == 'DKFM':
            t = t[:,0,:]
            if additional_data == 'None':
                logits_, test_loss_op= session.run([logits, loss_op], feed_dict={X_user:t[:,0], X_item:t[:,1], 
                                            X_ctxt:t[:,2:t.shape[1]-1], Y_labels: np.reshape(t[:,t.shape[1]-1], (-1,1))})
            else:
                logits_, test_loss_op= session.run([logits, loss_op], feed_dict={X_user:t[:,0], X_item:t[:,1], 
                                    user_item_features:t[:,2:t.shape[1] - dataset.dim_ctxt - 1],
                                    X_ctxt:t[:,2+dataset.dim_dfm:t.shape[1] - 1], 
                                    Y_labels: np.reshape(t[:,t.shape[1]-1], (-1,1))})
        elif args.model == 'DL':
            if additional_data == 'None':
                logits_, test_loss_op = session.run([logits, loss_op], 
                                                     feed_dict={X_user:t[:,0], X_item:t[:,1], Y_labels: np.reshape(t[:,2], (-1,1))})
            else:
                t = t[:,0,:]
                logits_, test_loss_op = session.run([logits, loss_op], 
                                           feed_dict={X_user:t[:,0], X_item:t[:,1], user_item_features:t[:,2:t.shape[1]-1],
                                                                            Y_labels:np.reshape(t[:,t.shape[1]-1], (len(t), 1))})

        _test_loss += test_loss_op 

        if args.model == 'BPRMF':
            count = 0
            for xi in x_:
                if(xi>0):
                    count += 1
                mrr +=  1/((len(x_) - count) + 1) 
                if len(x_) - count <= 10:
                    hitrate_10 += 1
        
        else:
            count = 0
            x_i = sigmoid(logits_[0])
            for t_i in logits_[1:len(logits_)]:
                x_j = sigmoid(t_i)
                if(x_i>=x_j):
                    count += 1
                mrr +=  1/((len(logits_) - count))
                if len(logits_) - count <= 10:
                    hitrate_10 += 1


        print("test_loss: ", (_test_loss/len(t_test)))
        print("hitrate@10 = ", (hitrate_10/dataset.nb_user))
        print("MRR = ", (mrr/dataset.nb_user))
        print("")
        tot_hitrate_10 += (hitrate_10/dataset.nb_user)
        tot_mrr += (mrr/dataset.nb_user)    
        tot_test_loss += (_test_loss/len(t_test))    
        

    print("test_loss: ", tot_test_loss/args.nb_round)
    print("hitrate@10 = ", tot_hitrate_10/args.nb_round)
    print("MRR = ", tot_mrr/args.nb_round)
    print("")
    